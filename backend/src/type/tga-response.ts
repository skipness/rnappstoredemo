type TopGrossingAppResponse = {
  feed?: Feed;
};

type Feed = {
  author: Author;
  entry: Entry[];
  updated: Label;
  rights: Label;
  title: Label;
  icon: Label;
  link: FeedLink[];
  id: Label;
};

type Author = {
  name: Label;
  uri: Label;
};

type Label = {
  label: string;
};

export type Entry = {
  'im:name': Label;
  'im:image': IMImage[];
  summary: Label;
  'im:price': IMPrice;
  'im:contentType': IMContentType;
  rights: Label;
  title: Label;
  link: EntryLink[];
  id: ID;
  'im:artist': IMArtist;
  category: Category;
  'im:releaseDate': IMReleaseDate;
};

type Category = {
  attributes: CategoryAttributes;
};

type CategoryAttributes = {
  'im:id': string;
  term: string;
  scheme: string;
  label: string;
};

type ID = {
  label: string;
  attributes: IDAttributes;
};

type IDAttributes = {
  'im:id': string;
  'im:bundleId': string;
};

type IMArtist = {
  label: string;
  attributes: IMArtistAttributes;
};

type IMArtistAttributes = {
  href: string;
};

type IMContentType = {
  attributes: IMContentTypeAttributes;
};

type IMContentTypeAttributes = {
  term: string;
  label: string;
};

type IMImage = {
  label: string;
  attributes: IMImageAttributes;
};

type IMImageAttributes = {
  height: string;
};

type IMPrice = {
  label: string;
  attributes: IMPriceAttributes;
};

type IMPriceAttributes = {
  amount: string;
  currency: string;
};

type IMReleaseDate = {
  label: Date;
  attributes: Label;
};

type EntryLink = {
  attributes: PurpleAttributes;
  'im:duration'?: Label;
};

type PurpleAttributes = {
  rel: string;
  type: string;
  href: string;
  title?: string;
  'im:assetType'?: string;
};

type FeedLink = {
  attributes: FluffyAttributes;
};

type FluffyAttributes = {
  rel: string;
  type?: string;
  href: string;
};

export default TopGrossingAppResponse;
