import cors from '@koa/cors';
import fs from 'fs';
import koa from 'koa';
import Router from 'koa-router';
import { fetchTopFreeAppJob, fetchTopGrossingJob } from './jobs';

const app = new koa();
const router = new Router();

app.use(cors({ origin: '*' }));
app.use(async (context, next) => {
  try {
    await next();
  } catch (error) {
    error.status = error.statusCode || error.status || 500;
    context.app.emit('error', error, context);
  }
});

fetchTopGrossingJob.start();
fetchTopFreeAppJob.start();
console.log(fetchTopGrossingJob.nextDates(), fetchTopFreeAppJob.nextDates());

router.get('/', (context) => {
  context.body = 'hi';
});

router.get('/gettopgrossing', (context) => {
  const data = fs.readFileSync('top-grossing-app.json');
  context.body = JSON.parse(data.toString());
});

router.get('/gettopfree/:start/:end', (context) => {
  const data = fs.readFileSync('top-free-app.json');
  context.body = {
    apps: JSON.parse(data.toString()).apps.slice(
      context.params.start,
      context.params.end
    ),
  };
});

app.use(router.routes());
app.listen(9999);
