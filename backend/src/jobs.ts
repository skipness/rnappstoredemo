import { CronJob } from 'cron';
import fs from 'fs';
import fetch from 'node-fetch';
import TopGrossingAppResponse, { Entry } from './type/tga-response';
import LookupResonse from './type/lku-response';

export const fetchTopGrossingJob = new CronJob(
  '0 0 */1 * * *',
  async () => {
    try {
      const response = await fetch(
        'https://itunes.apple.com/hk/rss/topgrossingapplications/limit=10/json'
      );
      const result: TopGrossingAppResponse = await response.json();
      const list = result.feed?.entry.map((entry: Entry) => ({
        appID: entry.id,
        appTitle: entry['im:name'].label,
        appCategory: entry.category.attributes.label,
        appSummary: entry.summary.label,
        thumbnail: entry['im:image'],
      }));
      fs.writeFileSync(
        'top-grossing-app.json',
        JSON.stringify({
          apps: list,
        })
      );
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  undefined,
  undefined,
  undefined,
  undefined,
  true
);

export const fetchTopFreeAppJob = new CronJob(
  '0 0 */1 * * *',
  async () => {
    try {
      const response = await fetch(
        'https://itunes.apple.com/hk/rss/topfreeapplications/limit=100/json'
      );
      const result: TopGrossingAppResponse = await response.json();
      if (result.feed) {
        const list = await Promise.all(
          result.feed.entry.map(async (entry: Entry, index: number) => {
            return await new Promise((resolve) =>
              setTimeout(async () => {
                const lookUpResponse = await fetch(
                  `https://itunes.apple.com/hk/lookup?id=${entry.id.attributes['im:id']}`
                );
                const lookUpResult: LookupResonse = await lookUpResponse.json();
                resolve({
                  appID: entry.id,
                  appTitle: entry['im:name'].label,
                  appCategory: entry.category.attributes.label,
                  appSummary: entry.summary.label,
                  index: index,
                  rating: lookUpResult.results[0].averageUserRating,
                  ratingCount: lookUpResult.results[0].userRatingCount,
                  thumbnail: entry['im:image'],
                });
              }, 10 * 1000)
            );
          })
        );
        fs.writeFileSync(
          'top-free-app.json',
          JSON.stringify({
            apps: list,
          })
        );
      }
    } catch (error) {
      console.log(error);
      throw error;
    }
  },
  undefined,
  undefined,
  undefined,
  undefined,
  true
);
