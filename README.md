# React Native App Store Demo

### How to run
```bash
cd backend
npm install
npm run dev
cd ..
cd frontend
npm install
cd ios
pod install
cd ..
npx react-native run-ios
```