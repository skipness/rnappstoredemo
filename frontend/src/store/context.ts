import { createContext } from 'react';
import { Action, AppState } from '../type/type';

export const initialState: AppState = {
  error: false,
  isLoading: true,
  isSearching: false,
  searchResult: [],
  topGrossingApp: [],
  topFreeApp: [],
};

export default createContext<{
  state: AppState;
  dispatch: React.Dispatch<Action>;
}>({ state: initialState, dispatch: () => null });
