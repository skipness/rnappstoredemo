import React, { ReactNode, useReducer } from 'react';
import Store, { initialState } from './context';
import reducer from './reducer';

type ProviderProps = { children: ReactNode };

const Provider = ({ children }: ProviderProps) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <Store.Provider value={{ state, dispatch }}>{children}</Store.Provider>
  );
};

export default Provider;
