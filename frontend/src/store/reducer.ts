import { Action, AppState } from '../type/type';

const reducer = (state: AppState, action: Action): AppState => {
  switch (action.type) {
    case 'FETCH_SUCCESS':
      const topGrossingApp = action.payload.topGrossingApp;
      const topFreeApp = action.payload.topFreeApp;
      return {
        ...state,
        error: false,
        isLoading: false,
        searchResult: [...topGrossingApp, ...topFreeApp],
        topGrossingApp: topGrossingApp,
        topFreeApp: topFreeApp,
      };
    case 'FETCH_FAIL':
      return { ...state, error: true, isLoading: false };
    case 'FETCH_MORE_FREE_APP_SUCCESS':
      return {
        ...state,
        topFreeApp: [...state.topFreeApp, ...action.payload],
      };
    case 'SEARCHING':
      return { ...state, isSearching: action.payload };
    case 'SET_SEARCH_RESULT':
      return { ...state, searchResult: action.payload };
    default:
      return state;
  }
};

export default reducer;
