import { APIResponse } from '../type/type';

export const fetchTopGrossing = async (): Promise<APIResponse> => {
  const response = await fetch('http://localhost:9999/gettopgrossing');
  const result: APIResponse = await response.json();
  return result;
};

export const fetchTopFreeApp = async (
  start: number,
  end: number
): Promise<APIResponse> => {
  const response = await fetch(
    `http://localhost:9999/gettopfree/${start}/${end}`
  );
  const result: APIResponse = await response.json();
  return result;
};
