export type Action =
  | {
      type: 'FETCH_SUCCESS';
      payload: {
        topGrossingApp: App[];
        topFreeApp: App[];
      };
    }
  | { type: 'FETCH_FAIL'; payload: boolean }
  | { type: 'FETCH_MORE_FREE_APP_SUCCESS'; payload: App[] }
  | { type: 'SEARCHING'; payload: boolean }
  | { type: 'SET_SEARCH_RESULT'; payload: App[] };

export type AppState = {
  error: boolean;
  isLoading: boolean;
  isSearching: boolean;
  searchResult: App[];
  topGrossingApp: App[];
  topFreeApp: App[];
};

export type APIResponse = {
  apps: App[];
};

export type App = {
  appID: ID;
  appTitle: string;
  appCategory: string;
  appSummary: string;
  index?: number;
  rating?: number;
  ratingCount?: number;
  thumbnail: Thumbnail[];
};

type ID = {
  label: string;
  attributes: IDAttributes;
};

type IDAttributes = {
  'im:id': string;
  'im:bundleId': string;
};

type Thumbnail = {
  label: string;
  attributes: IMAttributes;
};

type IMAttributes = {
  height: string;
};
