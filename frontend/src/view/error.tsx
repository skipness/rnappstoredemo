import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const Error = () => (
  <View style={styles.container}>
    <Text>出現錯錯誤</Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Error;
