import React, { useContext, useEffect } from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import SearchBar from '../component/search-bar';
import TopFreeAppList from '../component/top-free-app-list/top-free-app-list';
import { fetchTopFreeApp, fetchTopGrossing } from '../network/api';
import Context from '../store/context';
import Loading from '../view/loading';
import Error from './error';

const Home = () => {
  const {
    dispatch,
    state: { error, isLoading },
  } = useContext(Context);
  useEffect(() => {
    const fetchAllData = async () => {
      try {
        const result = await Promise.all([
          fetchTopGrossing(),
          fetchTopFreeApp(0, 10),
        ]);
        dispatch({
          type: 'FETCH_SUCCESS',
          payload: {
            topGrossingApp: result[0].apps,
            topFreeApp: result[1].apps,
          },
        });
      } catch (error) {
        console.log(error);
        dispatch({ type: 'FETCH_FAIL', payload: true });
      }
    };
    fetchAllData();
  }, []);
  return (
    <SafeAreaView style={styles.container}>
      {isLoading ? (
        <Loading />
      ) : error ? (
        <Error />
      ) : (
        <>
          <SearchBar />
          <TopFreeAppList />
        </>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({ container: { flex: 1 } });

export default Home;
