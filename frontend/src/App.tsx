import React from 'react';
import Provider from './store/provider';
import Home from './view/home';

const App = () => (
  <Provider>
    <Home />
  </Provider>
);

export default App;
