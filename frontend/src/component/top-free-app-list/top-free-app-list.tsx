import React, { useContext, useState } from 'react';
import { FlatList, Keyboard, Text, View } from 'react-native';
import { fetchTopFreeApp } from '../../network/api';
import Context from '../../store/context';
import { App } from '../../type/type';
import Loading from '../../view/loading';
import TopGrossingAppList from '../top-grossing-app-list/top-grossing-app-list';
import TopFreeAppListCell from './top-free-app-list-cell';

const TopFreeAppList = () => {
  const {
    dispatch,
    state: { isSearching, searchResult, topFreeApp },
  } = useContext(Context);
  const itemRenderer = ({ item, index }: { item: App; index: number }) => (
    <TopFreeAppListCell item={item} index={index} showSummary={isSearching} />
  );
  const [offset, setOffset] = useState(10);
  const onEndReached = async () => {
    if (offset === 100 || isSearching) return;
    const response = await fetchTopFreeApp(offset, offset + 10);
    dispatch({ type: 'FETCH_MORE_FREE_APP_SUCCESS', payload: response.apps });
    setOffset((offset: number) => offset + 10);
  };
  const emptyResult = (
    <View
      style={{
        backgroundColor: 'yellow',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Text>找不到相關的App</Text>
    </View>
  );
  return (
    <FlatList
      data={isSearching ? searchResult : topFreeApp}
      ItemSeparatorComponent={() => (
        <View
          style={{
            height: 1,
            backgroundColor: 'lightgrey',
            marginLeft: 16,
          }}
        />
      )}
      keyExtractor={(item: App) => item.appID.attributes['im:id']}
      ListEmptyComponent={emptyResult}
      ListHeaderComponent={isSearching ? null : <TopGrossingAppList />}
      ListFooterComponent={offset === 100 || isSearching ? null : <Loading />}
      onEndReached={onEndReached}
      onEndReachedThreshold={0.99}
      onScroll={Keyboard.dismiss}
      renderItem={itemRenderer}
    />
  );
};

export default TopFreeAppList;
