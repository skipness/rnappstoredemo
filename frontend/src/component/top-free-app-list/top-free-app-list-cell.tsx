import React, { useEffect, useRef } from 'react';
import {
  Animated,
  Linking,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { Rating } from 'react-native-ratings';
import { App } from '../../type/type';
import AppCategory from '../common/app-category';
import AppTitle from '../common/app-title';
import Thumbnail from '../common/thumbnail';

type TopFreeAppListCellProps = {
  index: number;
  showSummary: boolean;
  item: App;
};

const TopFreeAppListCell = ({
  item,
  index,
  showSummary,
}: TopFreeAppListCellProps) => {
  const fadeAnim = useRef(new Animated.Value(0)).current;
  const onPress = async () => {
    const canOpen = await Linking.canOpenURL(item.appID.label);
    if (canOpen) {
      Linking.openURL(item.appID.label);
    }
  };
  useEffect(() => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 1000,
      useNativeDriver: true,
    }).start();
  }, [fadeAnim]);
  return (
    <Animated.View style={{ opacity: fadeAnim }}>
      <TouchableOpacity onPress={onPress}>
        <View style={styles.container}>
          <Text style={styles.appIndex}>{index + 1}</Text>
          <Thumbnail
            circle={index % 2 === 0}
            size={80}
            uri={item.thumbnail[2].label}
          />
          <View style={styles.appInfoContainer}>
            <AppTitle title={item.appTitle} />
            <AppCategory category={item.appCategory} />
            {!showSummary && (
              <View style={styles.appRatingContainer}>
                <Rating
                  imageSize={12}
                  ratingColor='#ffa500'
                  readonly
                  startingValue={item.rating}
                  type='custom'
                />
                <Text style={styles.appRatingCount}>({item.ratingCount})</Text>
              </View>
            )}
            {showSummary && (
              <Text numberOfLines={2} style={styles.appSummaryText}>
                {item.appSummary}
              </Text>
            )}
          </View>
        </View>
      </TouchableOpacity>
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    height: 90,
    marginVertical: 10,
  },
  appIndex: { color: 'grey', fontSize: 24, paddingHorizontal: 16 },
  appInfoContainer: {
    alignSelf: 'stretch',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    paddingHorizontal: 16,
  },
  appRatingContainer: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  appRatingCount: {
    color: 'grey',
    fontSize: 12,
    marginLeft: 5,
  },
  appSummaryText: {
    color: 'grey',
  },
});

export default TopFreeAppListCell;
