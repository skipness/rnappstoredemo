import React from 'react';
import { Image } from 'react-native';

type ThumbnailProps = {
  circle?: boolean;
  size: number;
  uri: string;
};

const Thumbnail = ({ circle = false, size, uri }: ThumbnailProps) => {
  return (
    <Image
      style={{
        borderRadius: circle ? size / 2 : 22,
        height: size,
        width: size,
      }}
      source={{ uri: uri }}
    />
  );
};

export default Thumbnail;
