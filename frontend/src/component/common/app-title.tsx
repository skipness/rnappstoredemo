import React from 'react';
import { Text } from 'react-native';

type AppTitleProps = {
  title: string;
};

const AppTitle = ({ title }: AppTitleProps) => (
  <Text
    numberOfLines={2}
    style={{    
      fontSize: 14,
    }}
  >
    {title}
  </Text>
);

export default AppTitle;
