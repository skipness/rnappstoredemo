import React from 'react';
import { Text } from 'react-native';

type AppCategoryProps = {
  category: string;
};

const AppCategory = ({ category }: AppCategoryProps) => (
  <Text
    numberOfLines={1}
    style={{
      color: 'grey',
      fontSize: 14,
    }}
  >
    {category}
  </Text>
);

export default AppCategory;
