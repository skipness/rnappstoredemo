import React from 'react';
import { Linking, StyleSheet, TouchableOpacity, View } from 'react-native';
import { App } from '../../type/type';
import AppCategory from '../common/app-category';
import AppTitle from '../common/app-title';
import Thumbnail from '../common/thumbnail';

type TopGrossingAppListCellProps = {
  item: App;
};

const TopGrossingAppListCell = ({ item }: TopGrossingAppListCellProps) => {
  const onPress = async () => {
    const canOpen = await Linking.canOpenURL(item.appID.label);
    if (canOpen) {
      Linking.openURL(item.appID.label);
    }
  };
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={styles.container}>
        <Thumbnail size={100} uri={item.thumbnail[2].label} />
        <AppTitle title={item.appTitle} />
        <AppCategory category={item.appCategory} />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    height: 170,
    justifyContent: 'space-between',
    marginHorizontal: 8,
    marginVertical: 10,
    width: 100,
  },
});

export default TopGrossingAppListCell;
