import React, { useContext } from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import Context from '../../store/context';
import { App } from '../../type/type';
import TopGrossingAppListCell from './top-grossing-app-list-cell';

const GrossingAppList = () => {
  const {
    state: { topGrossingApp },
  } = useContext(Context);
  const itemRenderer = ({ item }: { item: App }) => (
    <TopGrossingAppListCell item={item} />
  );
  return (
    <View style={styles.container}>
      <Text style={styles.header}>推介</Text>
      <FlatList
        data={topGrossingApp}
        horizontal
        keyExtractor={(app: App) => app.appID.attributes['im:id']}
        renderItem={itemRenderer}
        showsHorizontalScrollIndicator={false}
      />
      <View style={styles.seperator} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 16,
  },
  header: {
    color: 'black',
    fontSize: 24,
    fontWeight: 'bold',
    paddingHorizontal: 16,
  },
  seperator: {
    backgroundColor: 'lightgrey',
    height: 1,
  },
});

export default GrossingAppList;
