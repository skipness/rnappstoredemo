import React, { useContext, useRef, useState } from 'react';
import {
  Image,
  Keyboard,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Search from '../assets/search.png';
import Context from '../store/context';
import { App } from '../type/type';

const SearchBar = () => {
  const {
    dispatch,
    state: { isSearching, topFreeApp, topGrossingApp },
  } = useContext(Context);
  const ref = useRef<TextInput>(null);
  const [text, setText] = useState('');
  const onCancelSearch = () => {
    dispatch({ type: 'SEARCHING', payload: false });
    ref.current && ref.current.blur();
  };
  const onChangeText = (text: string) => {
    const searchResult = [...topFreeApp, ...topGrossingApp].filter(
      (app: App) => {
        return app.appTitle.includes(text);
      }
    );
    dispatch({ type: 'SET_SEARCH_RESULT', payload: searchResult });
    setText(text);
  };
  const onFocus = () => dispatch({ type: 'SEARCHING', payload: true });
  return (
    <View style={styles.searchBarContainer}>
      <View style={styles.container}>
        <Image style={styles.icon} source={Search} />
        <TextInput
          onChangeText={onChangeText}
          onFocus={onFocus}
          onSubmitEditing={Keyboard.dismiss}
          placeholder='搜尋'
          ref={ref}
          returnKeyType='done'
          style={styles.input}
          value={text}
        />
      </View>
      {isSearching && (
        <TouchableOpacity style={styles.cancelButton} onPress={onCancelSearch}>
          <Text style={styles.cancelButtonText}>取消搜尋</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  searchBarContainer: {
    alignItems: 'center',
    backgroundColor: 'whitesmoke',
    flexDirection: 'row',
    height: 50,
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'gainsboro',
    borderRadius: 5,
    flexDirection: 'row',
    height: 30,
    justifyContent: 'center',
  },
  icon: {
    height: 10,
    marginRight: 5,
    width: 10,
  },
  input: {
    alignSelf: 'stretch',
    color: 'grey',
  },
  cancelButton: {
    marginLeft: 5,
  },
  cancelButtonText: {
    color: '#147EFB',
    fontSize: 14,
  },
});

export default SearchBar;
